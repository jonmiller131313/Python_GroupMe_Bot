#!/usr/bin/env python3

import json, requests
import sys

class Info:
    token = ""
    gUrl = "https://api.groupme.com/v3/groups?token=" 
    groupUrl = ""
    rUrl = "https://api.groupme.com/v3/bots?token="
    registerUrl = ""

    def setToken(t):
        Info.token = t
        Info.groupUrl = Info.gUrl + Info.token
        Info.registerUrl = Info.rUrl + Info.token

def prettyPrintJson(jsonData):
    print(json.dumps(jsonData, indent=4, separators=(',', ':')))


def getGroupChatData():
    response = requests.get(Info.groupUrl)
    return response.json()
    


def registerBot(groupChatData):
    print("Group chats:")
    length = len(groupChatData["response"])
    if length == 0:
        print("There are no group chats!")
        return

    i = -1
    while i + 1 < length:
        i += 1
        chat = groupChatData["response"][i]
        print("%d. %s\n   (group_id: %s)"  %(i + 1, chat["name"], chat["group_id"]))

    select = None
    while select == None:
        select = input("\nNumber of group chat to register bot: ")
        try:
            if int(select) < 1 or int(select) > length:
                print("Please input a value in the bounds.")
                select = None
        except ValueError:
            print("Please input a number")
            select = None

    name = input("Name of the bot: ")
    packet = {"bot": {"name": name, "group_id": groupChatData["response"][int(select) - 1]["group_id"]}}

    sys.stdout.write("Sending... ")
    data = requests.post(Info.registerUrl, json=packet)
    print("Response: %s" %(data.json()["meta"]["code"]))




def main():
    t = input("API Access Token: ")
    Info.setToken(t)
    d = getGroupChatData()
    print("Group chat request sent. Response: %d\n" %(d["meta"]["code"]))
    registerBot(d)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\nAbort")
        exit(1)
