# Groupme Python Bot
This is a personal bot for GroupMe I've written in Python.

## Setup
Make sure you have python3 install and pip:
```
sudo apt install python3 pip3
```

Install the pipenv python package and the requests package:
```bash
sudo -H pip3 install pipenv
pipenv install requests
```

## Resourses
 - The GroupMe bot API can be found [here](https://dev.groupme.com/docs/v3), and a tutorial can be found [here](https://dev.groupme.com/tutorials/bots).
 - The GroupMe user bot page can be found [here](https://dev.groupme.com/bots).
 - The requests API documentation can be found [here](http://docs.python-requests.org/en/master/), and [here](http://docs.python-requests.org/en/master/user/quickstart/) is the quickstart guide.
 